from flask import Flask, request, flash, url_for, redirect, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///students.sqlite3'
app.config['SECRET_KEY'] = "random string"

db = SQLAlchemy(app)

class users(db.Model):
    id = db.Column('user_id', db.Integer, primary_key = True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    password = db.Column(db.String(100))

    def __init__(self, first_name, last_name, email, password):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/signUp')
def signUp():
    return render_template('signUp.html')

@app.route('/submit_login')
def submit_login():
    email = request.args.get('email')
    password = request.args.get('password')
    user = users.query.filter_by(email=email).first()

    if not hasattr(user, 'id'):
        flash('User Does not Exist, Try Again')
        return render_template('login.html')

    if user.password == password:
        return render_template('secretPage.html')
    else:
        flash('Login Failed, Please Retry again')
        return render_template('login.html')

@app.route('/submit_sign_up')
def submit_sign_up():
    first_name = request.args.get('first_name')
    last_name = request.args.get('last_name')
    email = request.args.get('email')
    password = request.args.get('password')
    confirm_password = request.args.get('confirm_password')

    errors = False;
    if not any(char.islower() for char in password):
        errors = True
        flash('Password should have at least one lowercase letter')

    if not any(char.isupper() for char in password):
        errors = True
        flash('Password should have at least one uppercase letter')

    if not (password[-1].isdigit()):
        errors = True
        flash('Password should end with a number')

    if errors:
        return render_template('signUp.html')

    user = users.query.filter_by(email=email).first()
    if hasattr(user, 'id'):
        flash('username already exist')
        return render_template('signUp.html')

    if confirm_password != password:
        flash('Password and Confirm Password Does not Match, Please Try Again')
        return render_template('signUp.html')

    new_user = users(first_name, last_name, email, password)
    db.session.add(new_user)
    db.session.commit()
    flash('Record was successfully added')
    return render_template('home.html')

@app.route('/show_all')
def show_all():
    return render_template('show_all.html', students = users.query.all() )

if __name__ == '__main__':
    db.create_all()
    app.run(debug = True)
