from main import db, Student

db.create_all()

sai = Student('sai', 100)
sam = Student ('Sam', 105)

print(sam.id)
print(sai.id)

db.session.add_all([sai, sam])

db.session.commit()

print(sai.id)
print(sam.id)
